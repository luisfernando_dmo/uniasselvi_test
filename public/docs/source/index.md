---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_2be1f0e022faf424f18f30275e61416e -->
## Authenticate user

> Example request:

```bash
curl -X POST "http://localhost/api/v1/auth/login" \
    -H "Content-Type: application/json" \
    -d '{"email":"rerum","password":"consequuntur"}'

```
```javascript
const url = new URL("http://localhost/api/v1/auth/login");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "rerum",
    "password": "consequuntur"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (400):

```json
{
    "message": {
        "email": [
            "O campo email é obrigatório."
        ],
        "password": [
            "O campo password é obrigatório."
        ]
    }
}
```

### HTTP Request
`POST api/v1/auth/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | -
    password | string |  required  | -

<!-- END_2be1f0e022faf424f18f30275e61416e -->

<!-- START_3157fb6d77831463001829403e201c3e -->
## Register user

> Example request:

```bash
curl -X POST "http://localhost/api/v1/auth/register" \
    -H "Content-Type: application/json" \
    -d '{"name":"doloremque","email":"dignissimos","cpf":"culpa","password":"ut"}'

```
```javascript
const url = new URL("http://localhost/api/v1/auth/register");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "doloremque",
    "email": "dignissimos",
    "cpf": "culpa",
    "password": "ut"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (400):

```json
{
    "message": {
        "name": [
            "O campo name é obrigatório."
        ],
        "email": [
            "O campo email é obrigatório."
        ],
        "cpf": [
            "O campo cpf é obrigatório."
        ],
        "password": [
            "O campo password é obrigatório."
        ]
    }
}
```

### HTTP Request
`POST api/v1/auth/register`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | -
    email | string |  required  | -
    cpf | string |  required  | -
    password | string |  required  | -

<!-- END_3157fb6d77831463001829403e201c3e -->

<!-- START_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->
## Display a listing of the users

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user" 
```
```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/user`


<!-- END_d7f5c16f3f30bc08c462dbfe4b62c6b9 -->

<!-- START_96b8840d06e94c53a87e83e9edfb44eb -->
## Store a newly created user

> Example request:

```bash
curl -X POST "http://localhost/api/v1/user" 
```
```javascript
const url = new URL("http://localhost/api/v1/user");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`POST api/v1/user`


<!-- END_96b8840d06e94c53a87e83e9edfb44eb -->

<!-- START_a8f148df1f2cd4bc2d67314d2cb9fa3d -->
## Display the specified user

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/user/{user}`


<!-- END_a8f148df1f2cd4bc2d67314d2cb9fa3d -->

<!-- START_1006d782d67bb58039bde349972eb2f0 -->
## Update the specified user

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`PUT api/v1/user/{user}`

`PATCH api/v1/user/{user}`


<!-- END_1006d782d67bb58039bde349972eb2f0 -->

<!-- START_a5d7655acadc1b6c97d48e68f1e87be9 -->
## Remove the specified user

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/user/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/user/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/user/{user}`


<!-- END_a5d7655acadc1b6c97d48e68f1e87be9 -->

<!-- START_83123d72dc3e2c81c54272c523ebb840 -->
## Display a listing of the products

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/product" 
```
```javascript
const url = new URL("http://localhost/api/v1/product");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/product`


<!-- END_83123d72dc3e2c81c54272c523ebb840 -->

<!-- START_dad65efa4de78aa41631136d2aa6b536 -->
## Store a newly created product

> Example request:

```bash
curl -X POST "http://localhost/api/v1/product" \
    -H "Content-Type: application/json" \
    -d '{"name":"quia","bar_code":"necessitatibus","price":1493074,"amount":11}'

```
```javascript
const url = new URL("http://localhost/api/v1/product");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "quia",
    "bar_code": "necessitatibus",
    "price": 1493074,
    "amount": 11
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`POST api/v1/product`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  required  | -
    bar_code | string |  required  | -
    price | float |  required  | -
    amount | integer |  required  | -

<!-- END_dad65efa4de78aa41631136d2aa6b536 -->

<!-- START_0fd715dbf31ee69492f47793ab887f31 -->
## Display the specified product

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/product/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/product/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/product/{product}`


<!-- END_0fd715dbf31ee69492f47793ab887f31 -->

<!-- START_9c4cab1f29241c002b45740ca398bf36 -->
## Update the specified product

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/product/1" \
    -H "Content-Type: application/json" \
    -d '{"name":"dolorem","bar_code":"eaque","price":4944.19653839,"amount":14}'

```
```javascript
const url = new URL("http://localhost/api/v1/product/1");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "name": "dolorem",
    "bar_code": "eaque",
    "price": 4944.19653839,
    "amount": 14
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`PUT api/v1/product/{product}`

`PATCH api/v1/product/{product}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    name | string |  optional  | -
    bar_code | string |  optional  | -
    price | float |  optional  | -
    amount | integer |  optional  | -

<!-- END_9c4cab1f29241c002b45740ca398bf36 -->

<!-- START_1fa28fab968bebdbc21f5f7154a5085a -->
## Remove the specified product

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/product/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/product/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/product/{product}`


<!-- END_1fa28fab968bebdbc21f5f7154a5085a -->

<!-- START_08fe67c2133d6e6b8441c327a46987c4 -->
## Display a listing of the orders

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/order" 
```
```javascript
const url = new URL("http://localhost/api/v1/order");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/order`


<!-- END_08fe67c2133d6e6b8441c327a46987c4 -->

<!-- START_dccb15eb3789546b33d63f236c413842 -->
## Store a newly created order

> Example request:

```bash
curl -X POST "http://localhost/api/v1/order" \
    -H "Content-Type: application/json" \
    -d '{"client_id":19,"product_id":10,"status_id":6,"amount":10,"discount":20}'

```
```javascript
const url = new URL("http://localhost/api/v1/order");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "client_id": 19,
    "product_id": 10,
    "status_id": 6,
    "amount": 10,
    "discount": 20
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`POST api/v1/order`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    client_id | integer |  required  | -
    product_id | integer |  required  | -
    status_id | integer |  required  | -
    amount | integer |  required  | -
    discount | integer |  optional  | -

<!-- END_dccb15eb3789546b33d63f236c413842 -->

<!-- START_a51a526000224c34bc713c1c9fc24ebe -->
## Display the specified order

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/order/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/order/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/order/{order}`


<!-- END_a51a526000224c34bc713c1c9fc24ebe -->

<!-- START_f4f27fa43a87594f1502160858bd36d6 -->
## Update the specified order

> Example request:

```bash
curl -X PUT "http://localhost/api/v1/order/1" \
    -H "Content-Type: application/json" \
    -d '{"status_id":19}'

```
```javascript
const url = new URL("http://localhost/api/v1/order/1");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "status_id": 19
}

fetch(url, {
    method: "PUT",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`PUT api/v1/order/{order}`

`PATCH api/v1/order/{order}`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    status_id | integer |  optional  | -

<!-- END_f4f27fa43a87594f1502160858bd36d6 -->

<!-- START_0e6c1a4bb37b87686ab56cb7174c0d3a -->
## Remove the specified order

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/order/1" 
```
```javascript
const url = new URL("http://localhost/api/v1/order/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/order/{order}`


<!-- END_0e6c1a4bb37b87686ab56cb7174c0d3a -->

<!-- START_3ab4d7754472397e018957fa8110ac8c -->
## Logout

> Example request:

```bash
curl -X GET -G "http://localhost/api/v1/logout" 
```
```javascript
const url = new URL("http://localhost/api/v1/logout");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`GET api/v1/logout`


<!-- END_3ab4d7754472397e018957fa8110ac8c -->

<!-- START_fb5e13f0a4e3edb0480dd97e1a801fd7 -->
## Remove multiple products

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/ajax/product/destroy_in_large_scale" \
    -H "Content-Type: application/json" \
    -d '{"remove":[]}'

```
```javascript
const url = new URL("http://localhost/api/v1/ajax/product/destroy_in_large_scale");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "remove": []
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/ajax/product/destroy_in_large_scale`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    remove | array |  optional  | ex: "remove": [11, 12]

<!-- END_fb5e13f0a4e3edb0480dd97e1a801fd7 -->

<!-- START_03343e437bc4a679d98c3f454a6e6a6e -->
## Remove multiple users

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/ajax/user/destroy_in_large_scale" \
    -H "Content-Type: application/json" \
    -d '{"remove":[]}'

```
```javascript
const url = new URL("http://localhost/api/v1/ajax/user/destroy_in_large_scale");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "remove": []
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/ajax/user/destroy_in_large_scale`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    remove | array |  optional  | ex: "remove": [11, 12]

<!-- END_03343e437bc4a679d98c3f454a6e6a6e -->

<!-- START_d9f111341826f86ff298c7067cf2d9e3 -->
## Remove multiple orders

> Example request:

```bash
curl -X DELETE "http://localhost/api/v1/ajax/order/destroy_in_large_scale" \
    -H "Content-Type: application/json" \
    -d '{"remove":[]}'

```
```javascript
const url = new URL("http://localhost/api/v1/ajax/order/destroy_in_large_scale");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "remove": []
}

fetch(url, {
    method: "DELETE",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Token not provided"
}
```

### HTTP Request
`DELETE api/v1/ajax/order/destroy_in_large_scale`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    remove | array |  optional  | ex: "remove": [11, 12]

<!-- END_d9f111341826f86ff298c7067cf2d9e3 -->


