$(document).ready(function(){
    $('[name=cpf]').mask('000.000.000-00');
    $('[name=price]').mask("#.##0,00", {reverse: true});

    $(function () {
        $('#orders').DataTable();
        $('#clients').DataTable();
        $('#products').DataTable();
    });

    let remove = [];

    /* Products */
    $('#remove_several').click(function (e) {
        remove = [];

        $.each($('input[name=remove]:checked'), function(){
            remove.push($(this).val());
        });

        if(remove.length > 0){
            $('#myModalDelete').modal('show');
        }
    });

    $('#m_delete').click(function () {
        $('#myModalDelete').modal('hide');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'DELETE',
            url:'/admin/ajax/product/destroy_in_large_scale',
            data: {'remove': remove},
            success:function(data){
                if(data.message){
                    bootbox.alert('Produto(s) removido(s) com sucesso', function () {
                        window.location.reload();
                    });
                }
            }
        });
    });

    /* Clients */
    $('#remove_several_user').click(function (e) {
        remove = [];

        $.each($('input[name=remove_user]:checked'), function(){
            remove.push($(this).val());
        });

        if(remove.length > 0){
            $('#myModalDeleteUsers').modal('show');
        }
    });

    $('#m_delete_user').click(function () {
        $('#myModalDeleteUsers').modal('hide');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'DELETE',
            url:'/admin/ajax/user/destroy_in_large_scale',
            data: {'remove': remove},
            success:function(data){
                if(data.message){
                    bootbox.alert('Cliente(s) removido(s) com sucesso', function () {
                        window.location.reload();
                    });
                }
            }
        });
    });

    /* Orders */
    $('#remove_several_order').click(function (e) {
        remove = [];

        $.each($('input[name=remove_order]:checked'), function(){
            remove.push($(this).val());
        });

        if(remove.length > 0){
            $('#myModalDeleteOrders').modal('show');
        }
    });

    $('#m_delete_order').click(function () {
        $('#myModalDeleteOrders').modal('hide');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type:'DELETE',
            url:'/admin/ajax/order/destroy_in_large_scale',
            data: {'remove': remove},
            success:function(data){
                if(data.message){
                    bootbox.alert('Pedido(s) removido(s) com sucesso', function () {
                        window.location.reload();
                    });
                }
            }
        });
    });
});

function ajaxDiscount(object){
    const id = object.value;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type:'GET',
        url:'/admin/ajax/user/' + id,
        success:function(data){

            $('#info').show();
            $('#user_info_view').show();
            $('#user_name_info').text(data.user.name);
            $('#user_cpf_info').text(data.user.cpf).mask('000.000.000-00');

            if(data.discount > 0){
                $('#bday').show();
                $('#discount').val(data.discount);
            }else{
                $('#bday').hide();
                $('#discount').val(0);
            }
        }
    });
}

function ajaxProductPrice(object) {
    const id = object.value;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type:'GET',
        url:'/admin/ajax/product/' + id,
        success:function(data){
            $('#info').show();
            $('#product_info_view').show();
            $('#product_price_info').text(data.product.price).mask("#.##0,00", {reverse: true});
            $('#product_name_info').text(data.product.name + ' | Em estoque: ' + data.product.amount);

        }
    });
}

function setPageEditProduct(id) {
    window.location.href = window.location.pathname +'/' +  id + '/edit';
}

function setEditModalIndexAdmin(id){
    $('#statusModal' + id).modal('show');
}

function setPageEditClient(id) {
    window.location.href = window.location.pathname +'/' +  id + '/edit';
}