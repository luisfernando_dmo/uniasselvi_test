@extends('admin.layouts.app')
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> Todos</h3>
            </div>
            @if(Session::has('success'))
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                </div>
            @endif
        <!-- /.box-header -->
            <div class="box-body">
                <!-- Tabela com usuários-->
                @if(!$orders->isEmpty())
                    <table id="orders" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>DEL (escala)</th>
                            <th>Número do pedido</th>
                            <th>Cliente</th>
                            <th>Produto</th>
                            <th>Quantidade</th>
                            <th>Valor (R$)</th>
                            <th>Data do pedido</th>
                            <th>Status</th>
                            <th>DEL</th>
                            <th>EDT</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td><input name="remove_order" type="checkbox" value="{{$order->id}}"/></td>
                                <td>
                                    #{{$order->id}}
                                </td>
                                <td>
                                    <a href="{{route('admin::client.show', $order->user->id)}}">{{$order->user->name}}</a>
                                </td>
                                <td>
                                    <a href="{{route('admin::product.show', $order->product->id)}}"> {{$order->product->name}}</a>
                                </td>
                                <td>
                                    {{$order->amount}}
                                </td>
                                <td>
                                    {{$order->discount > 0 ? number_format($order->total, 2, ',', '.') . ' (' . $order->discount . '% off)' : number_format($order->total, 2, ',', '.')}}
                                </td>
                                <td>
                                    {{$order->created_at->format('d/m/Y H:m')}}
                                </td>
                                <td>
                                    @if($order->status_id == 1)
                                        <span class="label label-warning">Em aberto</span>
                                    @elseif($order->status_id == 2)
                                        <span class="label label-success">Pago</span>
                                    @else
                                        <span class="label label-danger">Cancelado</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="#" class="btn btn-danger" role="button"  data-toggle="modal" data-target="#modalDelete{{$order->id}}{{$order->name}}"><i class="fa fa-trash"></i></a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-success" role="button"  data-toggle="modal" data-target="#modalEdit{{$order->id}}{{$order->name}}"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="modalDelete{{$order->id}}{{$order->name}}" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Pedido Nº #{{$order->id}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('admin::order.destroy', $order->id)}}" method="post">
                                                <div class="col-md-offset-3">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                </div>
                                                    <p>Deseja realmente excluir este pedido?</p>
                                                    <button type="submit" class="btn btn-danger btn-block">Excluir</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="modalEdit{{$order->id}}{{$order->name}}" tabindex="-1" role="dialog" aria-labelledby="statusModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Editar Pedido Nº #{{$order->id}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('admin::order.update', $order->id)}}" method="post">
                                                    <input type="hidden" name="_method" value="PUT">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                                    <div class="box-body">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Cliente</label>
                                                                <select  disabled required name="client" class="form-control select2" style="width: 100%;">
                                                                    <option disabled selected value>Selecione o cliente</option>
                                                                    @foreach($clients as $client)
                                                                        <option value="{{$client->id}}" {{$order->user->id == $client->id ? 'selected' : ''}}>{{$client->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Produto</label>
                                                                <select disabled onchange="ajaxProductPrice(this);" required name="product" class="form-control select2" style="width: 100%;">
                                                                    <option disabled selected value>Selecione o produto</option>
                                                                    @foreach($products as $product)
                                                                        <option value="{{$product->id}}" {{$order->product->id == $product->id ? 'selected' : ''}}>{{$product->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Status</label>
                                                                <select required name="status" class="form-control select2" style="width: 100%;">
                                                                    <option disabled selected value>Selecione o status</option>
                                                                    @foreach($status as $s)
                                                                        <option value="{{$s->id}}" {{$order->status_id == $s->id ? 'selected' : ''}}>{{$s->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">Quantidade</label>
                                                                <input disabled required name="amount" type="number" value="{{$order->amount}}" class="form-control" id="exampleInputEmail1">
                                                            </div>
                                                        </div>
                                                            @if($order->discount > 0)
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Desconto no mês de aniversário (&#37;)</label>
                                                                        <input disabled id="discount" name="discount" value="{{$order->discount}}" type="number" class="form-control">
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    <!-- /.box-body -->
                                                    <div class="box-footer text-center">
                                                        <button type="submit" class="btn btn-primary">Editar</button>
                                                    </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                    <button id="remove_several_order" class="btn btn-danger">Deletar em escala</button>
                </div>
            @else
                <div class="alert alert-info" role="alert">
                    <i class="fa fa-info-circle"></i> Nenhum pedido cadastrado.
                </div>
            @endif
        </div>
        <div class="modal fade" id="myModalDeleteOrders" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Excluir o(s) pedido(s) selecionado(s)?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button id="m_delete_order" class="btn btn-danger">Excluir</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection