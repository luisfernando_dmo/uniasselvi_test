@extends('admin.layouts.app')
@section('content')
    <style>
        textarea {
            resize: none;
        }
    </style>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Cadastro</h3>
            </div>
            @if(Session::has('success'))
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="box-body">
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="sr-only">Error:</span>
                        @foreach($errors->all() as $error)
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {{$error}}<br>
                        @endforeach
                    </div>
                </div>
            @endif
            <div class="box-body">
                <form @if($data['type'] == 'edit') action="{{route($data['route'], $data['id'])}}" @else action="{{route($data['route'])}}" @endif method="{{$data['method']}}">
                    @if($data['type'] == 'edit')
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    @else
                    <!-- CSRF Token -->
                        {{ csrf_field() }}
                    @endif
                    @if(!$clients->isEmpty() && !$products->isEmpty())
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Clientes</label>
                                <select  onchange="ajaxDiscount(this);" required name="client" class="form-control select2" style="width: 100%;">
                                    <option disabled selected value>Selecione o cliente</option>
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}" {{$data['type'] == 'edit' && $data['client'] == $client->id ? 'selected' : ''}}>{{$client->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Produtos</label>
                                <select onchange="ajaxProductPrice(this);" required name="product" class="form-control select2" style="width: 100%;">
                                    <option disabled selected value>Selecione o produto</option>
                                    @foreach($products as $product)
                                        <option @if($product->amount == 0) disabled @endif value="{{$product->id}}" {{$data['type'] == 'edit' && $data['product'] == $product->id ? 'selected' : ''}}>{{$product->amount > 0 ? $product->name :  $product->name . ' (sem estoque)'}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Status</label>
                                <select required name="status" class="form-control select2" style="width: 100%;">
                                    <option disabled selected value>Selecione o status</option>
                                    @foreach($status as $s)
                                        <option value="{{$s->id}}" {{$data['type'] == 'edit' && $data['status'] == $s->id ? 'selected' : ''}}>{{$s->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Quantidade</label>
                                <input required name="amount" type="number" value="{{$data['type'] == 'create' ? old('amount') : $data['amount']}}" class="form-control" id="exampleInputEmail1">
                            </div>

                            <div hidden id="bday" class="form-group">
                                <label for="exampleInputEmail1">Desconto no mês de aniversário (&#37;) - <span style="color: gray;">[Sugestão do sistema]</span></label>
                                <input id="discount" name="discount" type="number" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label hidden id="info" for="exampleInputEmail1">Informativo</label>
                            <div hidden id="user_info_view" class="panel panel-default card-white">
                                <div class="divider-vertical-success">
                                    <div class="panel-body text-black text-center">
                                        <h2 id="user_name_info"></h2>
                                        <p id="user_cpf_info" class="text-uppercase"><b></b></p>
                                    </div>
                                </div>
                            </div>

                            <div hidden id="product_info_view" class="panel panel-default card-white">
                                <div class="divider-vertical-wait">
                                    <div class="panel-body text-black text-center">
                                        <h2 id="product_price_info"></h2>
                                        <p id="product_name_info" class="text-uppercase"><b></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                        <button type="submit" class="btn btn-primary">@if($data['type'] == 'create')Cadastrar @else Editar @endif</button>
                        @if($data['type'] == 'edit')
                            &nbsp;&nbsp;&nbsp;<a href="#" class="btn btn-danger" role="button"  data-toggle="modal" data-target="#myModalExcluir{{$data['id']}}">Excluir</a>
                        @endif
                    </div>
                </form>
            @if($data['type'] == 'edit')
                <!--Exluir-->
                    <div class="modal fade" id="myModalExcluir{{$data['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Excluir pedido para - {{$data['name']}}</h5>
                                </div>
                                <form action="{{route('admin::order.destroy', $data['id'])}}" method="POST">
                                    <div class="modal-body">
                                        <div class="form-group">Você realmente deseja excluir este pedido?</div>
                                        <div class="row">
                                            <div class="col-md-offset-3">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-danger">Excluir</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
                @if($data['type'] == 'edit')
                    <div class="box-body">
                        <a href="{{route('admin::order.index')}}" class="link"><i class="fa fa-angle-left"></i> Voltar</a>
                    </div>
                @endif
            </div>
            @else
                <div class="box-body">
                    <div class="alert alert-info alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-info-circle"></i> Antes de fazer um pedido é necessário cadastrar clientes e produtos.
                    </div>
                </div>
            @endif
            @if($data['type'] != 'edit')
                <div class="box-body pull-right">
                    <a href="{{route('admin::order.index')}}" class="link">  Listar todos <i class="fa fa-angle-right"></i></a>
                </div>
            @endif
        </div>
    </div>
@endsection