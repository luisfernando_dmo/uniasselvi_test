@extends('admin.layouts.app')
@section('content')
    <style>
        textarea {
            resize: none;
        }
    </style>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Visualização</h3>
            </div>
            <div class="box-body">
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome</label>
                            <input disabled required name="name" type="text" value="{{$client->name}}" class="form-control" id="exampleInputEmail1">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-mail</label>
                            <input disabled name="email" required type="text" class="form-control" value="{{$client->email}}" id="exampleInputEmail1"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">CPF</label>
                            <input disabled required name="cpf" type="text" class="form-control" value="  {{preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $client->cpf)}}" id="exampleInputEmail1"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Data de nascimento</label>
                            <input disabled required name="birthday" type="date" class="form-control" value="{{$client->birthday}}" id="exampleInputEmail1"/>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <a href="{{route('admin::order.index')}}" class="link"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>
@endsection