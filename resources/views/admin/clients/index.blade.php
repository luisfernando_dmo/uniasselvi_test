@extends('admin.layouts.app')
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> Todos</h3>
            </div>
            @if(Session::has('success'))
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                </div>
            @endif
            @if(count($errors) > 0)
                <div class="box-body">
                    <div class="alert alert-warning" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <span class="sr-only">Error:</span>
                        @foreach($errors->all() as $error)
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {{$error}}<br>
                        @endforeach
                    </div>
                </div>
            @endif
            <!-- /.box-header -->
            <div class="box-body">
                <!-- Tabela com usuários-->
                @if(!$clients->isEmpty())
                    <table id="clients" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>DEL (escala)</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>CPF</th>
                            <th>Data de nascimento</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr onmouseover="this.style.cursor='pointer'">
                                <td><input name="remove_user" type="checkbox" value="{{$client->id}}"/></td>
                                <td onclick="setPageEditClient({{$client->id}})">
                                    {{$client->name}}
                                </td>
                                <td onclick="setPageEditClient({{$client->id}})">
                                    {{$client->email}}
                                </td>
                                <td onclick="setPageEditClient({{$client->id}})">
                                    {{preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $client->cpf)}}
                                </td>
                                <td onclick="setPageEditClient({{$client->id}})">
                                    {{$client->birthday ? date('d/m/Y', strtotime($client->birthday)) : '-'}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <button id="remove_several_user" class="btn btn-danger">Deletar em escala</button>
            </div>
            @else
                <div class="alert alert-info" role="alert">
                    <i class="fa fa-info-circle"></i> Nenhum cliente cadastrado.
                </div>
            @endif
        </div>
        <div class="modal fade" id="myModalDeleteUsers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Excluir o(s) cliente(s) selecionado(s)?</h5>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button id="m_delete_user" class="btn btn-danger">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection