@extends('admin.layouts.app')
@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> Todos</h3>
            </div>
            @if(Session::has('success'))
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{Session::get('success')}}
                    </div>
                </div>
            @endif
        <!-- /.box-header -->
            <div class="box-body">
                @if(!$products->isEmpty())
                    <table id="products" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>DEL (escala)</th>
                            <th>Nome</th>
                            <th>Código de barras</th>
                            <th>Preço (R$)</th>
                            <th>Quantidade</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr onmouseover="this.style.cursor='pointer'">
                                <td><input name="remove" type="checkbox" value="{{$product->id}}"/></td>
                                <td onclick="setPageEditProduct({{$product->id}})">
                                    {{$product->name}}
                                </td>
                                <td onclick="setPageEditProduct({{$product->id}})">
                                    {{$product->bar_code}}
                                </td>
                                <td onclick="setPageEditProduct({{$product->id}})">
                                    {{number_format($product->price, 2, ',', '.')}}
                                </td>
                                <td onclick="setPageEditProduct({{$product->id}})">
                                    {{$product->amount}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <button id="remove_several" class="btn btn-danger">Deletar em escala</button>
            </div>
            @else
                <div class="alert alert-info" role="alert">
                    <i class="fa fa-info-circle"></i> Nenhum produto cadastrado.
                </div>
            @endif
        </div>

        <div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Excluir o(s) produto(s) selecionado(s)?</h5>
                    </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button id="m_delete" class="btn btn-danger">Excluir</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection