@extends('admin.layouts.app')
@section('content')
    <style>
        textarea {
            resize: none;
        }
    </style>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Visualização</h3>
            </div>
            <div class="box-body">
                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nome</label>
                                <input disabled required name="name" type="text" value="{{$product->name}}" class="form-control" id="exampleInputEmail1">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Código de barras</label>
                                <input disabled name="bar_code" required type="text" class="form-control" value="{{$product->bar_code}}" id="exampleInputEmail1"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Preço unitário (R$)</label>
                                <input disabled required name="price" type="text" class="form-control" value="{{number_format($product->price, 2, ',', '.')}}" id="exampleInputEmail1"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Quantidade</label>
                                <input disabled required name="amount" type="number" class="form-control" value="{{$product->amount}}" id="exampleInputEmail1"/>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <a href="{{route('admin::order.index')}}" class="link"><i class="fa fa-angle-left"></i> Voltar</a>
                    </div>
            </div>
        </div>
    </div>
@endsection