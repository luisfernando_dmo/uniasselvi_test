<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Http\Requests\OrderRequest;
use App\Order;
use App\Product;
use App\Status;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.orders.index', [
            'page' => Constants::PageOrder,
            'clients' =>  User::where('role_id','<>',  2)->get(),
            'status' => Status::all(),
            'products' => Product::all(),
            'orders' => Order::with('user')->with('product')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data = [
            'route' => 'admin::order.store',
            'method' => 'POST',
            'type' => 'create'
        ];

        return view('admin.orders.form', [
            'page' => Constants::PageOrder,
            'data' => $data,
            'clients' =>  User::where('role_id','<>',  2)->get(),
            'status' => Status::all(),
            'products' => Product::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $product = Product::find($request->product);

        $price = $product->price;

        $discount = $request->discount;

        if($product->amount > 0 && $product->amount >= $request->amount){

            if($discount > 0){
                $price -= (($price * $discount)/100);
            }

            $price *= $request->amount;

            try{
                DB::beginTransaction();

                $order = Order::create([
                    'user_id' => $request->client,
                    'product_id' => $request->product,
                    'status_id' => $request->status,
                    'discount' => $discount,
                    'amount' => $request->amount,
                    'total' => $price,
                ]);

                $product->amount -= $request->amount;
                $product->save();


                if($order && $product){
                    DB::commit();
                    return redirect()->route('admin::order.create')->with('success', 'Pedido cadastrado com sucesso! O valor total do seu pedido foi de R$ ' . number_format($price, 2, ',', '.'));
                }else{
                    DB::rollBack();
                    return redirect()->route('admin::order.create')->withErrors(['Erro ao cadastrar pedido! Por favor tente novamente mais tarde.']);
                }

            }catch (\Illuminate\Database\QueryException $e){
                return redirect()->route('admin::order.create')->withErrors(['Erro ao cadastrar pedido! Por favor tente novamente mais tarde.']);
            }

        }else{
            return redirect()->back()->withErrors(['Quantida informada maior do que a disponível']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        try{
            $order->status_id = $request->status;
            $order->save();

            return redirect()->route('admin::order.index', $id)->with('success', 'Pedido alterado com sucesso.');

        }catch (\Exception $e){
            return redirect()->route('admin::order.index')->withErrors(['Erro ao alterar pedido! Por favor tente novamente mais tarde.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        try{
            $order->delete();
            return redirect()->route('admin::order.index', $id)->with('success', 'Pedido excluido com sucesso.');
        }catch (\Illuminate\Database\QueryException $e){
            return redirect()->route('admin::order.index')->withErrors(['Erro ao excluir pedido! Por favor tente novamente mais tarde.']);

        }
    }
}