<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Http\Requests\ProductRequest;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index', [
            'page' => Constants::PageProduct,
            'products' => Product::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'route' => 'admin::product.store',
            'method' => 'POST',
            'type' => 'create'
        ];
        return view('admin.products.form', ['page' => Constants::PageProduct, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {

        try{
            Product::create([
                'name' => $request->name,
                'bar_code' => $request->bar_code,
                'price' => Constants::formatMoney($request->price),
                'amount' => $request->amount,
            ]);

            return redirect()->route('admin::product.create')->with('success', 'Produto cadastrado com sucesso.');

        }catch (\Illuminate\Database\QueryException $e){

            $barCodeError = strpos($e->getPrevious()->getMessage(), 'products_bar_code_unique');

            if($barCodeError){
                return redirect()->back()->withErrors(['Código de barras já está em uso']);

            }else{
                return redirect()->route('admin::product.create')->withErrors(['Erro ao cadastrar produto! Por favor tente novamente mais tarde.']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show',['page' => Constants::PageProduct, 'product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        $data = [
            'route' => 'admin::product.update',
            'id' => $id,
            'name' => $product->name,
            'bar_code' => $product->bar_code,
            'price' => $product->price,
            'amount' => $product->amount,
            'method' => 'POST',
            'type' => 'edit'
        ];

        return view('admin.products.form', ['page' => Constants::PageProduct, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        try{
            $product->name = $request->name;
            $product->bar_code = $request->bar_code;
            $product->price = Constants::formatMoney($request->price);
            $product->amount = $request->amount;

            $product->save();

            return redirect()->route('admin::product.index', $id)->with('success', 'Produto alterado com sucesso.');

        }catch (\Illuminate\Database\QueryException $e){
            $barCodeError = strpos($e->getPrevious()->getMessage(), 'products_bar_code_unique');

            if($barCodeError){
                return redirect()->back()->withErrors(['Código de barras já está em uso']);

            }else{
                return redirect()->route('admin::product.create')->withErrors(['Erro ao alterar produto! Por favor tente novamente mais tarde.']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        try{
            $product->delete();
            return redirect()->route('admin::product.index')->with('success', 'Produto excluído com sucesso.');

        }catch (\Exception $e){
            return redirect()->route('admin::product.index')->withErrors(['Erro ao excluir produto! Por favor tente novamente mais tarde.']);
        }
    }
}