<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        return view('admin.index', [
            'page' =>  Constants::PageDashboard,
            'clients' => User::where('role_id', '<>', 2)->count(),
            'products' => Product::all()->count(),
            'orders' => Order::all()->count(),
        ]);
    }
}