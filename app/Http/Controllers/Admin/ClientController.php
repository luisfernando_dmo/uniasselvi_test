<?php

namespace App\Http\Controllers\Admin;

use App\Constants;
use App\Http\Requests\ClientRequest;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.clients.index', [
            'page' => Constants::PageClient,
            'clients' => User::where('role_id','<>',  2)->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'route' => 'admin::client.store',
            'method' => 'POST',
            'type' => 'create'
        ];
        return view('admin.clients.form', ['page' => Constants::PageClient, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientRequest $request)
    {

        try{
            User::create([
                'role_id' => Constants::RoleClient,
                'name' => $request->name,
                'email' => $request->email,
                'cpf' => Constants::unMask($request->cpf, 'cpf'),
                'birthday' => $request->birthday,
                'password' => bcrypt($request->password)
            ]);

            return redirect()->route('admin::client.create')->with('success', 'Cliente cadastrado com sucesso.');

        }catch (\Illuminate\Database\QueryException $e){
            $cpfError = strpos($e->getPrevious()->getMessage(), 'users_cpf_unique');

            if($cpfError){
                return redirect()->route('admin::client.create')->withErrors(['CPF já está em uso']);
            }else{
                return redirect()->route('admin::client.create')->withErrors(['Erro ao cadastrar cliente! Por favor tente novamente mais tarde.']);
            }
        }
    }

    /*
     * Display the specified resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.clients.show', ['page' => Constants::PageClient, 'client' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = User::find($id);

        $data = [
            'route' => 'admin::client.update',
            'id' => $id,
            'name' => $client->name,
            'email' => $client->email,
            'cpf' => $client->cpf,
            'birthday' => $client->birthday,
            'method' => 'POST',
            'type' => 'edit'
        ];

        return view('admin.clients.form', ['page' => Constants::PageClient, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        try{
            $user->name = $request->name;
            $user->email = $request->email;
            $user->cpf = Constants::unMask($request->cpf, 'cpf');
            $user->birthday = $request->birthday;

            if($request->has('password') && !empty($request->password)){
                $user->password = bcrypt($request->password);
            }

            $user->save();

            return redirect()->route('admin::client.index', $id)->with('success', 'Cliente alterado com sucesso.');

        }catch (\Illuminate\Database\QueryException $e){
            $cpfError = strpos($e->getPrevious()->getMessage(), 'users_cpf_unique');

            if($cpfError){
                return redirect()->back()->withErrors(['CPF já está em uso']);
            }else{
                return redirect()->route('admin::client.index')->withErrors(['Erro ao editar cliente! Por favor tente novamente mais tarde.']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        try{
            $orders = Order::where('user_id', $user->id)->get();

            if($orders){
                foreach ($orders as $order){
                    $product = Product::find($order->product_id);
                    $product->amount += $order->amount;
                    $product->save();
                }
            }

            $user->delete();

            return redirect()->route('admin::client.index')->with('success', 'Cliente excluído com sucesso.');

        }catch (\Exception $e){
            return redirect()->route('admin::client.index')->withErrors(['Erro ao excluir cliente! Por favor tente novamente mais tarde.']);
        }
    }
}