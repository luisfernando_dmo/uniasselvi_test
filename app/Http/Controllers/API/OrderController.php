<?php

namespace App\Http\Controllers\API;

use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the orders
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = 20;

        if($request->query->has('per_page')) {
            $per_page = $request->query->get('per_page');
        }

        return response()->json(Order::with('user')->with('product')->paginate($per_page), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created order
     *
     * @bodyParam client_id int required -
     * @bodyParam product_id int required -
     * @bodyParam status_id int required -
     * @bodyParam amount int required -
     * @bodyParam discount int -
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'client_id' => 'required|numeric',
            'product_id' => 'required|numeric',
            'status_id' => 'required|numeric',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        $product = Product::find($request->product_id);

        $price = $product->price;

        $discount = $request->has('discount') ? $request->discount : 0;

        if($product->amount > 0 && $product->amount >= $request->amount) {

            if ($discount > 0) {
                $price -= (($price * $discount) / 100);
            }

            $price *= $request->amount;

            try {
                DB::beginTransaction();

                $order = Order::create([
                    'user_id' => $request->client_id,
                    'product_id' => $request->product_id,
                    'status_id' => $request->status_id,
                    'discount' => $discount,
                    'amount' => $request->amount,
                    'total' => $price,
                ]);

                $product->amount -= $request->amount;
                $product->save();


                if ($order && $product) {
                    DB::commit();
                    return response()->json($order, 200);
                } else {
                    DB::rollBack();
                    return response()->json(['message' => 'Erro ao cadastrar pedido! Por favor tente novamente mais tarde.'], 404);
                }

            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }else{
            return response()->json(['message' => 'Quantida informada maior do que a disponível'], 400);
        }
    }

    /**
     * Display the specified order
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with('user')->with('product')->find($id);

        if($order){
            return response()->json($order, 200);
        }else{
            return response()->json(['message' => 'order not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified order
     *
     * @bodyParam status_id int  -
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if(!$order){
            return response()->json(['message' => 'order not found'], 400);
        }

        try{
            $order->status_id = $request->has('status_id') ? $request->status_id : $order->status_id;
            $order->save();

            return response()->json($order, 200);

        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified order
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

        if(!$order){
            return response()->json(['message' => 'order not found'], 400);
        }

        try{
            $order->delete();
            return response()->json(['message' => 'order deleted with success'], 200);
        }catch (\Illuminate\Database\QueryException $e){
            return response()->json(['message' => $e->getMessage()], 500);

        }
    }
}