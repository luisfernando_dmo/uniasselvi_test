<?php

namespace App\Http\Controllers\API;

use App\Constants;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the products
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = 20;

        if($request->query->has('per_page')) {
            $per_page = $request->query->get('per_page');
        }

        return response()->json(Product::paginate($per_page), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created product
     *
     * @bodyParam name string required -
     * @bodyParam bar_code string required -
     * @bodyParam price double required -
     * @bodyParam amount int required -
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'bar_code' => 'required',
            'price' => 'required',
            'amount' => 'required|numeric'

        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        try{
            $product = Product::create([
                'name' => $request->name,
                'bar_code' => $request->bar_code,
                'price' => Constants::formatMoney($request->price),
                'amount' => $request->amount,
            ]);

            return response()->json($product, 200);

        }catch (\Illuminate\Database\QueryException $e){
            $barCodeError = strpos($e->getPrevious()->getMessage(), 'products_bar_code_unique');

            if($barCodeError){
                return response()->json(['message' => 'Código de barras já está em uso'], 400);
            }else{
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Display the specified product
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);

        if($product){
            return response()->json($product, 200);
        }else{
            return response()->json(['message' => 'product not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified product
     *
     * @bodyParam name string  -
     * @bodyParam bar_code string  -
     * @bodyParam price double  -
     * @bodyParam amount int  -
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);

        if(!$product){
            return response()->json(['message' => 'product not found'], 400);
        }

        try{
            $product->name = $request->name;
            $product->bar_code = $request->bar_code;
            $product->price = Constants::formatMoney($request->price);
            $product->amount = $request->amount;

            $product->save();

            return response()->json($product, 200);

        }catch (\Illuminate\Database\QueryException $e){
            $barCodeError = strpos($e->getPrevious()->getMessage(), 'products_bar_code_unique');

            if($barCodeError){
                return response()->json(['message' => 'Código de barras já está em uso'], 400);
            }else{
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Remove the specified product
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if(!$product){
            return response()->json(['message' => 'product not found'], 400);
        }

        try{
            $product->delete();
            return response()->json(['message' => 'product deleted with success'], 200);

        }catch (\Exception $e){
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}