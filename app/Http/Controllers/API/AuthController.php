<?php

namespace App\Http\Controllers\API;

use App\Constants;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Authenticate user
     *
     * @bodyParam email string required -
     * @bodyParam password string required -
     *
     */
    public function login(Request $request){

        $validator =  Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'

        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        $user = User::where('email', $request->email)->first();


        if(!$user){
            return response()->json(['message' => 'User not found'], 400);
        }

        if(!Hash::check($request->password, $user->password)){
            return response()->json(['message' => 'Password incorrect'], 400);
        }

        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'Could not create token'], 500);
        }

        $data = [
            'user' => $user,
            'type' => 'bearer',
            'token' => $token
        ];

        return response()->json($data, 200);
    }

    /**
     * Register user
     *
     * @bodyParam name string required -
     * @bodyParam email string required -
     * @bodyParam cpf string required -
     * @bodyParam password string required -
     *
     */
    public function register(Request $request){

        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'cpf' => 'required|cpf',
            'password' => 'required'

        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        try{
            $user = User::create([
                'role_id' => Constants::RoleClient,
                'name' => $request->name,
                'email' => $request->email,
                'cpf' => Constants::unMask($request->cpf, 'cpf'),
                'birthday' => $request->birthday,
                'password' => bcrypt($request->password)
            ]);

            $data = [
                'user' => $user,
                'type' => 'bearer',
                'token' => JWTAuth::fromUser($user)
            ];

            return response()->json($data, 200);

        }catch (\Illuminate\Database\QueryException $e){
            $cpfError = strpos($e->getPrevious()->getMessage(), 'users_cpf_unique');

            if($cpfError){
                return response()->json(['message' => ['cpf' => ['CPF já está em uso']]], 400);
            }else{
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function logout(){
        Auth::guard('api')->logout();
        return response()->json(['message' => 'Logout success'], 200);
    }
}