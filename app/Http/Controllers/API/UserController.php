<?php

namespace App\Http\Controllers\API;

use App\Constants;
use App\Http\Requests\ClientRequest;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* ADMIN */
        if(Auth::user()->role_id == 2){

            $per_page = 20;

            if($request->query->has('per_page')) {
                $per_page = $request->query->get('per_page');
            }

            return response()->json(User::where('role_id', '<>', 2)->paginate($per_page), 200);
        }else{
            return response()->json(['message' => 'administrator route'], 401);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'cpf' => 'required|cpf',
            'password' => 'required'

        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        try{
            $user = User::create([
                'role_id' => Constants::RoleClient,
                'name' => $request->name,
                'email' => $request->email,
                'cpf' => Constants::unMask($request->cpf, 'cpf'),
                'birthday' => $request->birthday,
                'password' => bcrypt($request->password)
            ]);

            return response()->json($user, 200);

        }catch (\Illuminate\Database\QueryException $e){
            $cpfError = strpos($e->getPrevious()->getMessage(), 'users_cpf_unique');

            if($cpfError){
                return response()->json(['message' => ['cpf' => ['CPF já está em uso']]], 400);
            }else{
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Display the specified user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if($user){
            return response()->json($user, 200);
        }else{
            return response()->json(['message' => 'user not found'], 400);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified user
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator =  Validator::make($request->all(), [
            'email' => 'required|email',
            'cpf' => 'required|cpf'
        ]);

        if($validator->fails()){
            return response()->json(['message' => $validator->messages()], 400);
        }

        $user = User::find($id);

        if(!$user){
            return response()->json(['message' => 'user not found'], 400);
        }

        try{
            $user->name = $request->name;
            $user->email = $request->email;
            $user->cpf = Constants::unMask($request->cpf, 'cpf');
            $user->birthday = $request->birthday;

            $user->save();

            return response()->json($user, 200);

        }catch (\Illuminate\Database\QueryException $e){
            $cpfError = strpos($e->getPrevious()->getMessage(), 'users_cpf_unique');

            if($cpfError){
                return response()->json(['message' => ['cpf' => ['CPF já está em uso']]], 400);
            }else{
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
    }

    /**
     * Remove the specified user
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /* ADMIN */
        if(Auth::user()->role_id == 2) {
            $user = User::find($id);

            if(!$user){
                return response()->json(['message' => 'user not found'], 400);
            }

            try {
                $orders = Order::where('user_id', $user->id)->get();

                if ($orders) {
                    foreach ($orders as $order) {
                        $product = Product::find($order->product_id);
                        $product->amount += $order->amount;
                        $product->save();
                    }
                }

                $user->delete();

                return response()->json(['message' => 'user deleted with success'], 200);

            } catch (\Illuminate\Database\QueryException $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }else{
            return response()->json(['message' => 'administrator route'], 401);
        }
    }
}
