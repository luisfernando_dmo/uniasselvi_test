<?php

namespace App\Http\Controllers;


use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function discount($id){
        $user = User::find($id);

        if($user){
            $monthBdayUser = substr($user->birthday, 5, 2);
            $monthNow = date('m');

            if($monthNow == $monthBdayUser){
                $discount = rand(5, 25);
            }else{
                $discount = 0;
            }

            return response()->json(['success' => true, 'discount' => $discount, 'user' => $user], 200);
        }else{
            return response()->json(['success' => false], 404);
        }

    }

    public function price($id){
        $product = Product::find($id);

        if($product){
            return response()->json(['success' => true, 'product' => $product], 200);
        }else{
            return response()->json(['success' => false], 404);
        }
    }

    /**
     * Remove multiple products
     * @bodyParam remove array  ex: "remove": [11, 12]
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyInLargeScaleProducts(Request $request){
        foreach ($request->remove as $id){

            $product = Product::find($id);

            try{
                $orders = Order::where('product_id', $product->id)->get();

                if($orders){
                    foreach ($orders as $order){
                        $order->delete();
                    }
                }
                $product->delete();
            }catch (\Exception $e){
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        return response()->json(['message' => true], 200);
    }

    /**
     * Remove multiple users
     * @bodyParam remove array  ex: "remove": [11, 12]
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyInLargeScaleUsers(Request $request){
        foreach ($request->remove as $id){

            $user = User::find($id);

            try{
                $orders = Order::where('user_id', $user->id)->get();

                if($orders){
                    foreach ($orders as $order){
                        $product = Product::find($order->product_id);
                        $product->amount += $order->amount;
                        $product->save();
                    }
                }
                $user->delete();
            }catch (\Exception $e){
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        return response()->json(['message' => true], 200);
    }

    /**
     * Remove multiple orders
     * @bodyParam remove array  ex: "remove": [11, 12]
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyInLargeScaleOrders(Request $request){
        foreach ($request->remove as $id){

            $order = Order::find($id);

            try{
                $order->delete();
            }catch (\Exception $e){
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }
        return response()->json(['message' => true], 200);
    }
}