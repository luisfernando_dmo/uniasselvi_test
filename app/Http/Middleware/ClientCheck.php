<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClientCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() && Auth::user()->role_id != 1) {
            return redirect('/');
        }

        /* Segundo caso, sessão expirou */
        if(!Auth::guard($guard)->check()){
            return redirect('/');
        }
        return $next($request);
    }
}