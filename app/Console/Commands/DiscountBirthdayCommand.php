<?php

namespace App\Console\Commands;

use App\Mail\SendMailDiscountInBirthdayMonthUser;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DiscountBirthdayCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'discount:birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending email with discount message in the user birthday month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        /* TODO: Send e-mail only once */
        $users = User::where('role_id', '<>', 2)->get();
        $monthNow = date('m');

        if($users){
            foreach ($users as $user){
                $monthBdayUser = substr($user->birthday, 5, 2);

                if($monthNow == $monthBdayUser){

                    $data = [
                        'user_name' => $user->name,
                        'user_email' => $user->email,
                        'discount' => rand(5, 25)
                    ];


                    Mail::send(new SendMailDiscountInBirthdayMonthUser($data));

                    if (Mail::failures()) {
                        $this->info('The discount message in the user [' . $user->name .'] birthday month not send!');
                    }else{
                        $this->info('The discount message in the user [' . $user->name . '] birthday month sent successfully!');
                    }
                }
            }
        }
    }
}