<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailDiscountInBirthdayMonthUser extends Mailable
{
    use Queueable, SerializesModels;

    private $data;

    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->subject = $data['user_name'] . ", no seu mês você tem até (" . $data['discount'] . "% off)";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.user.discount')->with([
            'name' => $this->data['user_name'],
            'discount' => $this->data['discount']
        ])->to($this->data['user_email']);
    }
}