<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constants extends Model
{
    const PageDashboard = 'Dashboard';
    const PageClient = 'Clientes';

    const PageProduct = 'Produtos';
    const PageOrder = 'Pedidos';

    const RoleClient = 1;

    public static function unMask($field, $type){
        $mask = null;

        switch ($type){
            case 'cpf':
                $mask = str_replace(['.', '-'],'', $field);
            break;

            case 'money':
                $mask = str_replace([','],'.', $field);
            break;
        }

        return $mask;
    }


    public static function formatMoney($money) {
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }
}