<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*  Authentication Routes */
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::post('login', 'Auth\LoginController@login');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

/* Groupo de rotas do admin */
Route::group(['as' => 'admin::', 'prefix' => 'admin', 'middleware' => ['admin']], function() {

    Route::get('dashboard', 'Admin\DashboardController@index')->name('dashboard.index');
    Route::resource('client', 'Admin\ClientController');
    Route::resource('product', 'Admin\ProductController');
    Route::resource('order', 'Admin\OrderController');
    Route::get('ajax/user/{id}', 'AjaxController@discount');
    Route::get('ajax/product/{id}', 'AjaxController@price');
    Route::delete('ajax/product/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleProducts');
    Route::delete('ajax/user/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleUsers');
    Route::delete('ajax/order/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleOrders');

});

/* Grupo de rotas do client */
Route::group(['as'=> 'client::', 'prefix' => 'client', 'middleware' => ['client']], function(){
    Route::get('dashboard', 'Client\DashboardController@index')->name('dashboard.index');
});