<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function() {
    Route::post('auth/login', 'API\AuthController@login');
    Route::post('auth/register', 'API\AuthController@register');
});

Route::group(['prefix' => 'v1', 'middleware' => 'jwt.auth'], function (){
    Route::resource('user', 'API\UserController', ['except' => ['create', 'edit']]);
    Route::resource('product', 'API\ProductController', ['except' => ['create', 'edit']]);
    Route::resource('order', 'API\OrderController', ['except' => ['create', 'edit']]);
    Route::get('logout', 'API\AuthController@logout');
    Route::get('status', function (){
       return response()->json(\App\Status::all(), 200);
    });

    Route::delete('ajax/product/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleProducts');
    Route::delete('ajax/user/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleUsers');
    Route::delete('ajax/order/destroy_in_large_scale', 'AjaxController@destroyInLargeScaleOrders');
});