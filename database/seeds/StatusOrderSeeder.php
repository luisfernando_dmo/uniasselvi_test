<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'name' => "em aberto",
        ]);

        DB::table('status')->insert([
            'name' => "pago",
        ]);

        DB::table('status')->insert([
            'name' => "cancelado",
        ]);
    }
}