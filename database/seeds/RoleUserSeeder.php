<?php

use Illuminate\Database\Seeder;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'type' => "client",
        ]);

        DB::table('roles')->insert([
            'type' => "admin",
        ]);
    }
}